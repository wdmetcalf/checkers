import java.awt.Dimension;
import javax.swing.JFrame;

public class MainWindow {
	private final int SQUARE_SIZE = 100;
	private JFrame window;
	private GameComponent gamePanel;
	@SuppressWarnings("unused")
	private Controller controller;

	public MainWindow() {
		// setup window
		this.window = new JFrame("Checkers");
		this.window.setVisible(true);
		this.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// add the main game panel
		this.gamePanel = new GameComponent(SQUARE_SIZE);
		this.gamePanel.setPreferredSize(new Dimension(SQUARE_SIZE * 8, SQUARE_SIZE * 8));
		this.window.add(gamePanel);
		this.window.pack();

		// start game
		this.controller = new Controller(gamePanel, SQUARE_SIZE);

	}

	public GameComponent getGameComponent() {
		return this.gamePanel;
	}
}
