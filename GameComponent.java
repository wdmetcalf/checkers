import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GameComponent extends JPanel {
	private int squareSize;
	private final Color SQ_C_1 = new Color(69, 69, 69);
	private final Color SQ_C_2 = new Color(135, 108, 0);
	private Piece[] player1 = null;
	private Piece[] player2 = null;
	private ArrayList<int[]> highlighted;

	public GameComponent(int size) {
		this.squareSize = size;
	}

	public void getPlayers(Piece[] player1, Piece[] player2) {
		System.out.println("Recieving players.");
		this.player1 = player1;
		this.player2 = player2;
	}

	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		drawBackground(g2);
		drawHighlighted(g2);

		if (player1 != null && player2 != null) {
			for (int i = 0; i < 12; i++) {
				player1[i].draw(g2, squareSize);
				player2[i].draw(g2, squareSize);
			}
		}
	}

	private void drawBackground(Graphics2D g2) {
		g2.setColor(SQ_C_1);
		for (int i = 0; i < 8; i += 2) {
			for (int j = 0; j < 8; j += 2) {
				Rectangle2D.Double rect = new Rectangle2D.Double(i * squareSize, j * squareSize, squareSize,
						squareSize);
				g2.fill(rect);
				g2.draw(rect);
			}
		}

		for (int i = 1; i < 8; i += 2) {
			for (int j = 1; j < 8; j += 2) {
				Rectangle2D.Double rect = new Rectangle2D.Double(i * squareSize, j * squareSize, squareSize,
						squareSize);
				g2.fill(rect);
				g2.draw(rect);
			}
		}

		g2.setColor(SQ_C_2);
		for (int i = 0; i < 8; i += 2) {
			for (int j = 1; j < 8; j += 2) {
				Rectangle2D.Double rect = new Rectangle2D.Double(i * squareSize, j * squareSize, squareSize,
						squareSize);
				g2.fill(rect);
				g2.draw(rect);
			}
		}

		for (int i = 1; i < 8; i += 2) {
			for (int j = 0; j < 8; j += 2) {
				Rectangle2D.Double rect = new Rectangle2D.Double(i * squareSize, j * squareSize, squareSize,
						squareSize);
				g2.fill(rect);
				g2.draw(rect);
			}
		}
	}

	public void drawHighlighted(Graphics2D g2) {
		g2.setColor(Color.CYAN);
		for (int[] a : highlighted) {
			Rectangle2D.Double rect = new Rectangle2D.Double(a[1] * squareSize, a[0] * squareSize, squareSize,
					squareSize);
			g2.fill(rect);
		}
	}

	public void addMouse(Controller listener) {
		this.addMouseListener(listener);
	}

	public void sendHighlighted(ArrayList<int[]> highlighted) {
		this.highlighted = highlighted;
	}
}
