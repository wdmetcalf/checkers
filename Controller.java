import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class Controller implements MouseListener {
	private int size;
	private Piece[] player1;
	private Piece[] player2;
	private GameComponent gamePanel;
	private Piece selected;
	private ArrayList<int[]> highlighted;

	public Controller(GameComponent game, int size) {
		this.gamePanel = game;
		this.size = size;
		this.highlighted = new ArrayList<int[]>();
		this.player1 = new Piece[12];
		this.player2 = new Piece[12];
		setupPieces(0, player1);
		setupPieces(1, player2);
		this.gamePanel.getPlayers(player1, player2);
		this.gamePanel.addMouse(this);
		this.gamePanel.sendHighlighted(highlighted);
		this.selected = null;
	}

	public Piece findPieceAtPos(int[] pos) {
		for (int i = 0; i < 12; i++) {
			int[] pos1 = player1[i].getPos();
			int[] pos2 = player2[i].getPos();
			if (pos1[0] == pos[0] && pos1[1] == pos[1] && player1[i].getIsAlive())
				return player1[i];
			if (pos2[0] == pos[0] && pos2[1] == pos[1] && player2[i].getIsAlive())
				return player2[i];
		}

		return null;
	}

	public void getPotentialMoves() {
		this.highlighted.clear();
		int[] pos = this.selected.getPos();
		int[] toTest;
		if (selected.getPlayer() == 0 || selected.getIsKing()) {
			// test down left
			toTest = new int[2];
			toTest[0] = pos[0] + 1;
			toTest[1] = pos[1] + 1;
			if (findPieceAtPos(toTest) == null) {
				highlighted.add(toTest);
			} else if (findPieceAtPos(toTest).getPlayer() != selected.getPlayer()) {
				toTest[0] += 1;
				toTest[1] += 1;
				if (findPieceAtPos(toTest) == null)
					highlighted.add(toTest);
			}
			// test down right
			toTest = new int[2];
			toTest[0] = pos[0] + 1;
			toTest[1] = pos[1] - 1;
			if (findPieceAtPos(toTest) == null) {
				highlighted.add(toTest);
			} else if (findPieceAtPos(toTest).getPlayer() != selected.getPlayer()) {
				toTest[0] += 1;
				toTest[1] -= 1;
				if (findPieceAtPos(toTest) == null)
					highlighted.add(toTest);
			}
		}

		if (selected.getPlayer() == 1 || selected.getIsKing()) {
			// test up left
			toTest = new int[2];
			toTest[0] = pos[0] - 1;
			toTest[1] = pos[1] + 1;
			if (findPieceAtPos(toTest) == null) {
				highlighted.add(toTest);
			} else if (findPieceAtPos(toTest).getPlayer() != selected.getPlayer()) {
				toTest[0] -= 1;
				toTest[1] += 1;
				if (findPieceAtPos(toTest) == null)
					highlighted.add(toTest);
			}
			// test up right
			toTest = new int[2];
			toTest[0] = pos[0] - 1;
			toTest[1] = pos[1] - 1;
			if (findPieceAtPos(toTest) == null) {
				highlighted.add(toTest);
			} else if (findPieceAtPos(toTest).getPlayer() != selected.getPlayer()) {
				toTest[0] -= 1;
				toTest[1] -= 1;
				if (findPieceAtPos(toTest) == null)
					highlighted.add(toTest);
			}
		}

	}

	public boolean attemptMove(int[] pos) {
		for (int[] a : highlighted)
			if (pos[0] == a[0] && pos[1] == a[1])
				return true;
		return false;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int[] click = { e.getY() / size, e.getX() / size };
		System.out.println("Clicked at: (" + click[0] + ", " + click[1] + ")");

		// reset the selected or move selected on every click
		if (this.selected != null) {
			if (attemptMove(click)) {
				int[] toKill = this.selected.move(click);
				if (toKill != null) {
					findPieceAtPos(toKill).kill();
				}
				this.selected.toggleSelect();
				this.selected = null;
				this.gamePanel.repaint();
				this.highlighted.clear();
				return;
			}
			this.selected.toggleSelect();
			this.highlighted.clear();
			this.gamePanel.repaint();
			this.selected = null;
		}
		this.highlighted.clear();

		Piece toSelect = this.findPieceAtPos(click);
		if (toSelect != null) {
			// if (turn % 2 == toSelect.getPlayer()) {
			if (toSelect != null) {
				this.selected = toSelect;
				this.selected.toggleSelect();
				this.getPotentialMoves();
			}
			this.gamePanel.repaint();
			// }
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}

	public void setupPieces(int player, Piece[] array) {
		if (player == 0) {
			int counter = 0;
			for (int i = 0; i < 3; i += 2) {
				for (int j = 0; j < 4; j++) {
					int[] pos = new int[2];
					pos[0] = i;
					pos[1] = j * 2;
					array[counter] = new Piece(player, pos);
					counter++;
				}
			}
			for (int i = 0; i < 4; i++) {
				int[] pos = new int[2];
				pos[0] = 1;
				pos[1] = i * 2 + 1;
				array[counter] = new Piece(player, pos);
				counter++;
			}
		} else if (player == 1) {
			int counter = 0;
			for (int i = 5; i < 8; i += 2) {
				for (int j = 0; j < 4; j++) {
					int[] pos = new int[2];
					pos[0] = i;
					pos[1] = j * 2 + 1;
					array[counter] = new Piece(player, pos);
					counter++;
				}
			}
			for (int i = 0; i < 4; i++) {
				int[] pos = new int[2];
				pos[0] = 6;
				pos[1] = i * 2;
				array[counter] = new Piece(player, pos);
				counter++;
			}
		} else
			throw new Error("Player perameter must be 0 or 1");
	}

	public static void main(String[] args) {
		// start the game
		new MainWindow();
	}
}
