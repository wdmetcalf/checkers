import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

public class Piece {
	private int team; // 0 black, 1 red
	private int[] position;
	private boolean isAlive;
	private boolean isKing;
	private boolean isSelected;
	static final Color RED = new Color(196, 33, 33);
	static final Color RED_KING = new Color(140, 1, 1);
	static final Color BLACK = new Color(38, 38, 38);
	static final Color BLACK_KING = new Color(0, 0, 0);

	public Piece(int team, int[] position) {
		this.team = team;
		this.position = position;
		this.isAlive = true;
		this.isKing = false;
		this.isSelected = false;
	}

	public void draw(Graphics2D g2, int size) {
		if (this.isAlive) {
			Ellipse2D.Double circ = new Ellipse2D.Double(this.position[1] * size, this.position[0] * size, size, size);
			// overly confusing way to choose the draw color, 4 ternary
			// operators ftw
			g2.setColor(this.isSelected ? Color.GREEN
					: (isKing ? (team == 1 ? RED_KING : BLACK_KING) : (team == 0 ? BLACK : RED)));
			g2.fill(circ);
		}
	}

	public void toggleSelect() {
		this.isSelected = !this.isSelected;
	}

	public String toString() {
		return "(" + position[0] + ", " + position[1] + ")";
	}

	public int[] getPos() {
		return this.position;
	}

	public int getPlayer() {
		return this.team;
	}

	public boolean getIsKing() {
		return this.isKing;
	}

	public void kingMe() {
		System.out.println("King me! " + this);
		this.isKing = true;
	}

	public int[] move(int[] click) {
		int[] mid = null;
		if (Math.abs(position[0] - click[0]) > 1 || Math.abs(position[1] - click[1]) > 1) {
			mid = new int[2];
			mid[0] = (position[0] + click[0]) / 2;
			mid[1] = (position[1] + click[1]) / 2;
		}
		this.position = click;
		if ((this.team == 0 && this.position[0] == 7) || (this.team == 1 && this.position[0] == 0))
			kingMe();
		return mid;
	}

	public boolean getIsAlive() {
		return this.isAlive;
	}

	public void kill() {
		this.isAlive = false;
	}
}
